<?php

namespace Tlf\LilDb\Test;

class Migrations extends \Tlf\Tester {

    public function testMigrate(){
        // @export_start(Example.Migration)
        $db = $this->file('test/input/migrate/db.sqlite');
        unlink($db);
        $migrations_dir = $this->file('test/input/migrate/');

        // init the database
        $ldb = \Tlf\LilDb::sqlite($db);
        $ldb->create('blog',
            ['title'=>'varchar(200)']
        );
        $ldb->insert('blog',['title'=>'one']);

        // do the migration
        $lm = new \Tlf\LilMigrations($ldb->pdo, $migrations_dir);
        $lm->migrate(1,2);

        // test that the table has been altered to have a 'description' field
        $this->compare(
            $ldb->select('blog')[0],
            ['title'=>'one',
            'description'=>'',
            ],
        );
        // @export_end(Example.Migration)
    }


    public function testMigrate2(){
        $db = $this->file('test/input/migrate/db.sqlite');
        unlink($db);
        $migrations_dir = $this->file('test/input/migrate/');

        $ldb = \Tlf\LilDb::sqlite($db);
        $ldb->create('blog',
            ['title'=>'varchar(200)']
        );
        $ldb->insert('blog',['title'=>'one']);

        $lm = new \Tlf\LilMigrations($ldb->pdo, $migrations_dir);

        $lm->migrate(1,3);
        $this->compare(
            $ldb->select('blog')[0],
            ['title'=>'one',
            'description'=>'',
            'id'=>null,
            ],
        );

    }

}
