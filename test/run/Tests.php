<?php

namespace Tlf\LilDb\Test;

class Tests extends \Tlf\Tester {

    public function testInsertEmptyRow(){
        $this->disable();

        echo "This test disabled because i can't figure out how to insert an empty row in sqlite. it seems to work fine in mysql, but mysql setup is more complicated so I don't want to test with mysql";

        return;
        $ldb = \Tlf\LilDb::sqlite();
        // $ldb = \Tlf\LilDb::mysql();
        $ldb->create('cat', ['id'=>'integer', 'name'=>'VARCHAR(50)', 'description'=>'VARCHAR(200)']);

        $ldb->insert('cat',[]);
    }

    public function testDeleteNonExistingRow(){
        $jeff = ['name'=>'Jeff', 'description'=>'A silly cat', 'id'=>0];
        $julie = ['name'=>'Julie', 'description'=>'A very silly cat','id'=>1];
        $ldb = \Tlf\LilDb::sqlite();
        // $ldb = \Tlf\LilDb::mysql();
        $ldb->create('cat', ['id'=>'integer', 'name'=>'VARCHAR(50)', 'description'=>'VARCHAR(200)']);

        $ldb->insert('cat',$jeff);
        $ldb->insert('cat',$julie);

        $response = $ldb->delete('cat', ['id'=>20]);
        
        $this->is_false($response);
    }

    public function testDelete(){
        $jeff = ['name'=>'Jeff', 'description'=>'A silly cat', 'id'=>0];
        $julie = ['name'=>'Julie', 'description'=>'A very silly cat','id'=>1];
        $ldb = \Tlf\LilDb::sqlite();
        // $ldb = \Tlf\LilDb::mysql();
        $ldb->create('cat', ['id'=>'integer', 'name'=>'VARCHAR(50)', 'description'=>'VARCHAR(200)']);

        $ldb->insert('cat',$jeff);
        $ldb->insert('cat',$julie);

        $response = $ldb->delete('cat', ['id'=>1]);
        
        $this->is_true($response);
    }

    public function testEverything(){
        $jeff = ['name'=>'Jeff', 'description'=>'A silly cat', 'id'=>0];
        $julie = ['name'=>'Julie', 'description'=>'A very silly cat','id'=>1];
        $ldb = \Tlf\LilDb::sqlite();
        // $ldb = \Tlf\LilDb::mysql();
        $ldb->create('cat', ['id'=>'integer', 'name'=>'VARCHAR(50)', 'description'=>'VARCHAR(200)']);

        $ldb->insert('cat',$jeff);
        $ldb->insert('cat',$julie);

        $jeff['id'] = 0;
        $julie['id'] = 1;

        $this->test('Insert');
            $this->compare(
                $jeff, $ldb->select('cat', ['name'=>$jeff['name']])[0]
            );
            $this->compare(
                $jeff, $ldb->select('cat', ['id'=>$jeff['id']])[0]
            );

        $julie['name'] = 'Julie The Great';
        $ldb->update('cat',$julie);

        $this->test('Update');
            $this->compare(
                $julie,
                $ldb->select('cat', ['id'=>$julie['id']])[0]
            );

        $this->test('Update Where');
        $julie['description'] = 'The most powerful cat.';
        $ldb->updateWhere('cat', $julie, ['name'=>$julie['name']]);
            $this->compare(
                $julie,
                $ldb->select('cat', ['name'=>$julie['name']])[0]
            );

        $this->compare(
            [$jeff,$julie],
            $ldb->select('cat',[])
        );

        $ldb->delete('cat',['name'=>$jeff['name']]);
        $this->compare(
            [$julie],
            $ldb->query('SELECT * FROM cat')
        );

        $this->compare(
            [$julie],
            $ldb->query("SELECT * FROM cat WHERE name LIKE :name", ['name'=>$julie['name']])
        );

        $ldb->insert('cat',$jeff);
        $ldb->insert('cat',['name'=>'beep','description'=>'boop']);
        $ldb->delete('cat',[]);
        $this->compare(
            [],
            $ldb->select('cat', [])
        );
    }

}
