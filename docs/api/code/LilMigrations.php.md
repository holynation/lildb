<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File code/LilMigrations.php  
  
# class Tlf\LilMigrations  
A minimal class for handlnig sql migrations  
  
  
  
## Constants  
  
## Properties  
- `public \PDO $pdo;` a pdo instance  
- `public string $dir;` the dir for migrations scripts.  
  
## Methods   
- `public function __construct(\PDO $pdo, string $dir)` In $dir, there should be directories named 'v1', 'v2', 'v3', and so on.  
In the v1/v2/v3 dirs, there should be up.sql & down.sql files with valid SQL statements for whatever database you're using  
  
- `static public function sqlite(string $dbName = ':memory:')` Convenience method to initialize sqlite db in memory  
- `public function migrate(int $old, int $new)` Migrate from old version to new  
  
  
