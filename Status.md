# LilDB Dev status

## march 1, 2022
- started LilMigrations class for fun

## LilSql
While the tests don't have any programmed pass/fail conditions, they are working as intended, from a visual inspection. I have not written sql in main.sql or update.sql, but both create.sql & delete.sql are loading as expected when running `pt LilSql`, which loads a file & serializes it before unserializing & outputting the results

## TODO (LilSql):
- clean up commented out code
- make the tests ACTUALLY PASS
- test exception when multiple queries of the same name
- document LilSql properly (write a test method & copy the method contents into the readme file)

## Other TODO
- at top-ish of readme, write overview of what this lib is/does (what each Lil class does)
- write proper documentation of LilDb & LilMigrations w/ examples (copied from test methods)


## NOTICE
code scrawl isn't working ... because of the lexer issues ... so basically just don't run code scrawl


## Next
- Write some really brief, simple tests

## Latest
- Updated code scrawl with a classMethod template & implemented it in the readme
- Define the methods I'd like to implement
- Iterate over each, document, & implement
