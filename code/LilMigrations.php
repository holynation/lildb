<?php

namespace Tlf;

/**
 * A minimal class for handlnig sql migrations
 *
 * @tagline Easy to use SQL Migrations from versioned directories
 */
class LilMigrations {

    /**
     * a pdo instance
     */
    public \PDO $pdo;

    /**
     * the dir for migrations scripts.
     */
    public string $dir;

    /**
     * In $dir, there should be directories named 'v1', 'v2', 'v3', and so on.
     * In the v1/v2/v3 dirs, there should be up.sql & down.sql files with valid SQL statements for whatever database you're using
     *
     * @param $pdo a pdo instance
     * @param $dir a directory path
     */
    public function __construct(\PDO $pdo, string $dir){
        $this->pdo = $pdo;
        $this->dir = $dir;
    }

    /**
     * Convenience method to initialize sqlite db in memory
     * @return Tlf\LilDb
     */
    static public function sqlite(string $dbName = ':memory:'){
        $pdo = new \PDO('sqlite:'.$dbName);
        $pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        $ldb = new static($pdo);
        return $ldb;
    }


    /**
     *
     * Migrate from old version to new
     *
     * @param $old the current version of the database
     * @param $new the new version of the database to go to
     */
    public function migrate(int $old, int $new){

        if ($old < $new){
            $file = 'up.sql';
        } else {
            $file = 'down.sql';
        }

        for ($i=$old;$i<$new;$i++){
            $dir = $this->dir.'/v'.$i;
            $exec_file = $dir.'/'.$file;
            if (!file_exists($exec_file))continue;
            $sql = file_get_contents($exec_file);
            $this->pdo->exec($sql);
        }

    }
}
